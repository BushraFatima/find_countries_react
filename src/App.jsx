import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./Home";
import CountryDetails from "./components/CountryDetails";
import Header from "./components/Header";

function App() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/country/:id" element={<CountryDetails />} />
      </Routes>
    </>
  );
}

export default App;
