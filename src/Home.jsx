import React, { useState, useEffect, useContext } from "react";
import CountriesCard from "./components/CountriesCard";
import CountrySearch from "./components/CountrySearch";
import CountryFilter from "./components/CountryFilter";
import ThemeContext from "./components/ThemeContext";
import "./Home.css";

// Function calculates required information and return all the required components.
function Home() {
  const { theme } = useContext(ThemeContext);
  const [countries, setCountries] = useState([]);
  const [searchCountry, setSearchCountry] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [selectedSubRegion, setSelectedSubRegion] = useState("");
  const [sortPop, setSortPop] = useState("");
  const [sortArea, setSortArea] = useState("");
  const sortList = ["Ascending", "Descending"];

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        setCountries(data);
        setLoading(false);
      })
      .catch((err) => {
        setError(err);
        setLoading(false);
      });
  }, []);

  let regSub = countries.reduce((cnObj, country) => {
    if (country.region in cnObj) {
      if (!cnObj[country.region].includes(country.subregion)) {
        cnObj[country.region].push(country.subregion);
      }
    } else {
      cnObj[country.region] = [country.subregion];
    }

    return cnObj;
  }, {});

  regSub["Antarctic"] = ["No Sub Regions"];

  const regions = Object.keys(regSub);

  const handleSearch = (searchCountry) => {
    setSearchCountry(searchCountry);
  };

  const handleRegionChange = (selectedRegion) => {
    setSelectedRegion(selectedRegion);
    setSelectedSubRegion("");
  };

  const handleSubRegionChange = (selectedSubRegion) => {
    setSelectedSubRegion(selectedSubRegion);
  };

  let filteredCountries = countries.filter(
    (country) =>
      country.name.common
        .toLowerCase()
        .split(" ")
        .join("")
        .includes(searchCountry.toLowerCase()) &&
      (selectedRegion === "" || country.region === selectedRegion)
  );

  if (selectedSubRegion !== "") {
    if (selectedRegion !== "Antarctic") {
      filteredCountries = filteredCountries.filter(
        (country) => country.subregion === selectedSubRegion
      );
    }
  }

  const handleSortByPopulation = (sortPop) => {
    setSortPop((old) => (old = sortPop));
    // setCountries(filteredCountries)
    setSortArea("");
    console.log(sortPop, sortArea);
  };

  const handleSortByArea = (sortArea) => {
    setSortArea((old) => (old = sortArea));
    setSortPop("");
    // setCountries(filteredCountries)
    console.log(sortPop, sortArea);
  };

  if (sortPop !== "") {
    if (sortPop === "Ascending") {
      filteredCountries.sort((p1, p2) =>
        p1.population > p2.population
          ? 1
          : p1.population < p2.population
          ? -1
          : 0
      );
    } else {
      filteredCountries.sort((p1, p2) =>
        p1.population < p2.population
          ? 1
          : p1.population > p2.population
          ? -1
          : 0
      );
    }
  }

  if (sortArea !== "") {
    if (sortArea === "Ascending") {
      filteredCountries.sort((a1, a2) =>
        a1.area > a2.area ? 1 : a1.area < a2.area ? -1 : 0
      );
    } else {
      filteredCountries.sort((a1, a2) =>
        a1.area < a2.area ? 1 : a1.area > a2.area ? -1 : 0
      );
    }
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div
      className="App"
      style={
        theme
          ? { backgroundColor: "#202d36", color: "white" }
          : { backgroundColor: "#f2f3f5", color: "black" }
      }
    >
      <div className="container">
        <div className="srch-sel">
          <CountrySearch onSearch={handleSearch} />
          <CountryFilter
            unSelect="Filter By Population"
            lists={sortList}
            onSelectedValue={handleSortByPopulation}
          />
          <CountryFilter
            unSelect="Filter By Area"
            lists={sortList}
            onSelectedValue={handleSortByArea}
          />
          <CountryFilter
            unSelect="Filter By region"
            lists={regions}
            onSelectedValue={handleRegionChange}
          />
          <CountryFilter
            unSelect="Filter By Subregion"
            lists={regSub[selectedRegion]}
            onSelectedValue={handleSubRegionChange}
          />
        </div>
        <CountriesCard filteredCountries={filteredCountries} />
      </div>
    </div>
  );
}

export default Home;
