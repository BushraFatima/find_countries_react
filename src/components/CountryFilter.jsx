import React, { useContext } from "react";
import "./CountryFilter.css";
import ThemeContext from "./ThemeContext";

function CountryFilter({ unSelect, lists, onSelectedValue }) {
  const { theme } = useContext(ThemeContext);
  const handleFilter = (e) => {
    const selectedValue = e.target.value;
    onSelectedValue(selectedValue);
  };

  return (
    <div className="filters">
      <form>
        <select
          className="filter-field"
          style={
            theme
              ? {
                  backgroundColor: "#2b3743",
                  color: "white",
                  boxShadow: "hsl(0, 1%, 10%) 0px 5px 10px",
                }
              : { backgroundColor: "white", color: "black" }
          }
          onChange={handleFilter}
        >
          <option value="">{unSelect}</option>
          {lists &&
            lists.map((ele) => (
              <option key={ele} value={ele}>
                {ele}
              </option>
            ))}
        </select>
      </form>
    </div>
  );
}

export default CountryFilter;
