import React, { useContext } from "react";
import "./CountriesCard.css";
import ThemeContext from "./ThemeContext";
import { Link } from "react-router-dom";

// Function to return  each country card element.
function CountriesCard({ filteredCountries }) {
  const { theme } = useContext(ThemeContext);

  return (
    <div className="countries">
      {filteredCountries.length === 0 ? (
        <p id="not-found" className="not-found">
          No such countries found
        </p>
      ) : (
        filteredCountries.map((country) => (
          <Link
            to={`/country/${country.ccn3}`}
            style={{ textDecoration: "none", color: "inherit" }}
          >
            <div
              key={country.ccn3}
              className="c-card"
              style={
                theme
                  ? {
                      backgroundColor: "#2b3743",
                      color: "white",
                      boxShadow: "hsl(0, 1%, 10%) 0px 5px 10px",
                    }
                  : { backgroundColor: "white", color: "black" }
              }
            >
              <img src={country.flags.png} alt={country.name.common} />
              <h4>{country.name.common}</h4>
              <p>
                <b>Area:</b> {country.area}
              </p>
              <p>
                <b>Population:</b> {country.population}
              </p>
              <p>
                <b>Region:</b> {country.region}
              </p>
              <p>
                <b>Capital:</b> {country.capital}
              </p>
            </div>
          </Link>
        ))
      )}
    </div>
  );
}

export default CountriesCard;
