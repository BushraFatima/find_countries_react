import React, { useEffect, useState, useContext } from "react";
import { Link, useParams } from "react-router-dom";
import { BsArrowLeft } from "react-icons/bs";
import ThemeContext from "./ThemeContext";
import "./CountryDetails.css";

function CountryDetails() {
  const [details, setDetails] = useState([]);
  const { id } = useParams();
  const { theme } = useContext(ThemeContext);

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/alpha/${id}`)
      .then((res) => res.json())
      .then((data) => setDetails(data))
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const currency = details[0]
    ? Object.entries(details[0].currencies)[0][1].name
    : "none";
  const languages = details[0]
    ? Object.values(details[0].languages).join(", ")
    : "none";

  return (
    <section
      className="country"
      style={
        theme
          ? { backgroundColor: "#202d36", color: "white" }
          : { backgroundColor: "#f2f3f5" }
      }
    >
      <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
        <button
          className="back"
          style={
            theme
              ? {
                  backgroundColor: "#2b3743",
                  color: "white",
                  boxShadow: "hsl(0, 1%, 10%) 0px 5px 10px",
                }
              : { backgroundColor: "white", color: "black" }
          }
        >
          <BsArrowLeft />
          Back Home
        </button>
      </Link>
      <div>
        <div className="country-inner">
          <div className="flag">
            <img src={details[0]?.flags.png} />
          </div>

          <div className="country-details">
            <h2>{details[0]?.name.common}</h2>
            <div className="c-details">
              <div>
                <h5>
                  Native Name: <span>{details[0]?.name.common}</span>
                </h5>
                <h5>
                  Population: <span>{details[0]?.population}</span>
                </h5>
                <h5>
                  Region: <span>{details[0]?.region}</span>
                </h5>
                <h5>
                  Sub Region: <span>{details[0]?.subregion}</span>
                </h5>
                <h5>
                  Capital: <span>{details[0]?.capital}</span>
                </h5>
              </div>

              <div>
                <h5>
                  Top Level Domain: <span>{details[0]?.tld.join(", ")}</span>
                </h5>
                <h5>
                  Currencies: <span>{currency}</span>
                </h5>
                <h5>
                  Languages: <span>{languages}</span>
                </h5>
              </div>
            </div>
            <div className="border-details">
              <h5>Border Countries: </h5>
              <div className="borders">
                {details[0] ? (
                  details[0].borders ? (
                    details[0].borders.map((border) => (
                      <button
                        className="b-country"
                        style={
                          theme
                            ? {
                                backgroundColor: "#2b3743",
                                color: "white",
                                borderColor: "#434345",
                              }
                            : { backgroundColor: "white", color: "black" }
                        }
                      >
                        {border}
                      </button>
                    ))
                  ) : (
                    <span> No border countries.</span>
                  )
                ) : (
                  "none"
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default CountryDetails;
