import React, { useContext } from "react";
import { BsMoon } from "react-icons/bs";
import "./Header.css";
import ThemeContext from "./ThemeContext";
import { BsMoonFill } from "react-icons/bs";

/* Function return Header Element */
function Header() {
  const { theme, setTheme } = useContext(ThemeContext);

  function handleClick() {
    setTheme(!theme);
  }
  return (
    <div
      className="header"
      style={
        theme
          ? { backgroundColor: "#2b3743", color: "white" }
          : { backgroundColor: "white", color: "black" }
      }
    >
      <h3>Where in the world?</h3>
      <div className="theme-mode">
        {theme ? <BsMoonFill /> : <BsMoon />}
        <button
          onClick={() => handleClick()}
          style={theme ? { color: "white" } : { color: "black" }}
        >
          Dark Mode
        </button>
      </div>
    </div>
  );
}

export default Header;
