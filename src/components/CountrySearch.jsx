import React, { useContext } from "react";
import { BsSearch } from "react-icons/bs";
import "./CountrySearch.css";
import ThemeContext from "./ThemeContext";

/* Function return Search element */
function CountrySearch({ onSearch }) {
  const { theme } = useContext(ThemeContext);
  const handleInputChange = (e) => {
    const searchCountry = e.target.value;
    onSearch(searchCountry);
  };

  return (
    <div
      className="search"
      style={
        theme
          ? {
              backgroundColor: "#2b3743",
              color: "white",
              boxShadow: "hsl(0, 1%, 10%) 0px 5px 10px",
            }
          : { backgroundColor: "white", color: "black" }
      }
    >
      <BsSearch className="srch-icon" />
      <input
        type="text"
        placeholder="Search Country"
        onChange={handleInputChange}
      />
    </div>
  );
}

export default CountrySearch;
