# Creating Countries Search & Filter App Using REACT JS #

## App Component ##

1. App.jsx - Contains Home element and route to Country details page.

## Home Component ##

1. Home.jsx - Contains all the components required to build the structure and functionalities for filtering countries data.
2. Home.css - Contains css styles used for styling App component and its content/items.

## Country Search Component ##

1. CountrySearch.jsx - Contains search element that receives input text (usually a country name) from the user.
2. CountrySearch.css - Contains css styles used for styling CountrySearch component and its content/items.

## Country Filter Component ##

1. CountryFilter.jsx - Contains filter container of select fields and its options list. Select Fields receive selected option from user. Select fields are :-

    - Filter by region.
    - Filter by subregion for selected Region.
    - Filter by population.
    - Filter by area.

2. CountryFilter.css - Contains css styles used for styling CountryFilter component and its content/items.

## Countries Card Component ##

1. CountriesCard.jsx - Contains container of countries cards. Returns country card for each country that are in list of filtered countries given by user.
2. CountriesCard.css - Contains css styles used for styling CountriesCard component and its content/items.

## Country Details Component ##

1. CountryDetails.jsx - Contains details of the country - flag img, name, capital, population, currencies, region, subregion, languages, tld, border countries etc.

2. CountryDetails.css - Contains css styles used for styling Country Details page and its content/items.

## ThemeContext ##

1. ThemeContext.jsx - Contains theme mode context that can be used by the main page and children components.
